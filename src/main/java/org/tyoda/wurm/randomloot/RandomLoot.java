/*
RandomLoot mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.randomloot;

import com.wurmonline.server.creatures.Communicator;
import org.gotti.wurmunlimited.modloader.interfaces.*;

import java.util.Properties;
import java.util.logging.Logger;

public class RandomLoot implements WurmServerMod, PlayerMessageListener, Configurable {
    public static final String version = "1.0";

    public static final Logger logger = Logger.getLogger(RandomLoot.class.getName());

    public static int distMeters = 15 * 4;
    public static boolean includeGmsInRolls = false;
    public static String command = "#choose";

    public String getVersion(){
        return version;
    }

    @Override
    public void configure(Properties p){
        distMeters = Integer.parseInt(p.getProperty("distMeters", String.valueOf(distMeters)));
        includeGmsInRolls = Boolean.parseBoolean(p.getProperty("includeGmsInRolls", String.valueOf(includeGmsInRolls)));
        command = p.getProperty("command", command);
        if(!command.startsWith("#")){
            command = '#'+command;
        }
    }

    @Override
    public MessagePolicy onPlayerMessage(Communicator communicator, String message, String title) {
        return LootMessageHandler.handleMessage(communicator, message) ? MessagePolicy.DISCARD : MessagePolicy.PASS;
    }

    @Override
    @Deprecated
    public boolean onPlayerMessage(Communicator communicator, String message) {
        return false;
    }
}